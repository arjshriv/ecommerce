import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import{Recipe} from '../recipes.model';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {
  
  @Output() recipeSelected=new EventEmitter<Recipe>();
  recipes:Recipe[]=[
    new Recipe('A Test Recipe','This is simply a Test','https://hips.hearstapps.com/del.h-cdn.co/assets/17/34/2048x1365/gallery-1503418862-chicken-thighs-delish.jpg'),
    new Recipe('A Test Another Recipe','This is simply a Test','https://hips.hearstapps.com/del.h-cdn.co/assets/17/34/2048x1365/gallery-1503418862-chicken-thighs-delish.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

  selectedARecipe(recipe:Recipe){
    this.recipeSelected.emit(recipe);
  }
}
