import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { Ingredients } from '../../shared/ingredients.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  /**
   * We are using Viewchild decorator to take  a value from template to componnent
   */
  @ViewChild('inputName') Name: ElementRef;
  @ViewChild('inputAmount') Amount: ElementRef;

  @Output() addItemInList = new EventEmitter<Ingredients>();
  constructor() { }

  ngOnInit() {
  }

  addItem() {
    const name = this.Name.nativeElement.value;
    const amount = this.Amount.nativeElement.value;
    this.addItemInList.emit(new Ingredients(name, amount));
  }
}
